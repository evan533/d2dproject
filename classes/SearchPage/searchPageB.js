import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity, TouchableWithoutFeedback,
    View,
    FlatList,
    Dimensions,
} from 'react-native';
import {Icon, Button, Item, Input,} from 'native-base';
import {detailsPage,AppScreen} from "../../screenNames";

const ITEM_WIDTH = Dimensions.get('window').width;
const ITEM_HEIGHT = Dimensions.get('window').height;

type Props = {};

class searchPageB extends Component<Props> {

    constructor(props) {
        super(props);

        this.state = {
            isLoading       : true,
            dataArray       : [],
            dataMounted     : false,
            toggle          : true,
            empty           : true,
            checked         : [],
            activeTab       : 1
        }
    }

    static defaultProps = {
        nameData: [
            {nama   : 'aza'          , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'bobi'         , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'caca'         , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'devi'         , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'evan'         , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'feli'         , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'gohan'        , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'haha'         , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'ivan'         , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'johan'        , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'kaleb'        , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'lemon'        , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'assaf'        , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'dbfbre'       , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'everbereran'  , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'erbrbrtb'     , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'gohrbeberan'  , id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'harberberha'  , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'ivdbdfvsdan'  , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'jorebervdhan' , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'kalsdvew eb'  , id: 'brand',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'lesdvevervmon', id: 'generik',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
        ]
    }

    componentDidMount() {
        for (let i = 0; i < this.props.nameData.length; i++) {
            this.state.dataArray.push(this.props.nameData[i]);
        }
    }

    _renderItem = (item) => {
        const {navigation} = this.props; 
        return (
            <TouchableOpacity onPress={() => {
                navigation.navigate(detailsPage,{name:item.item.nama,artikel:item.item.artikel,search:this.state.search})
                console.log(item.item.nama)
            }}>
                <View style={styles.item_obat}>
                    <Text style={styles.item_nama_obat}>{item.item.nama}{item.item.id}</Text>

                    <Text style={styles.item_tipe_obat}>{item.item.id}</Text>

                    <View style={styles.searchPage_Divider}></View>
                </View>
            </TouchableOpacity>
        );
    };

    updateSearch = search => {
        this.setState({search});
    };

    toggleBorder = function(){
        return{
            borderBottomWidth: 0.5,
            borderBottomColor: '#ffff59'
        }
    }

    onTabPress = (index) => {
        this.setState({activeTab: index, checked: []});
        setTimeout(() => {
            this.onFilterData(this.state.search);
        }, 500);
    }

    onFilterData = (keyword) => {
        let result = [];
        if (keyword) {
            keyword             = keyword.toLowerCase();

            let tab             = (this.state.activeTab == 2 ? 'generik' : (this.state.activeTab == 3 ? 'brand' : ''))
            let filterData      = this.state.dataArray.filter(obj => obj.nama.toLowerCase().includes(keyword));

            result              = filterData;
            if (tab) {
                result          = result.filter(obj => obj.id == tab);
            }
        }

        this.setState({
            checked: result,
            search: keyword
        });
    }

    clearInput = () => {
        this.state.search = ''
        this.textInput._root.clear()
        this.onFilterData(this.state.search)
    }

    render() {
        const {search}        = this.state;
        const {navigation}    = this.props;
        let sortedChecked     = this.state.checked.sort((a, b) => (a.nama > b.nama) ? 1 : -1);

        return (
            <View style={{flex: 1}}>
                <View>
                    {/* <SearchBar
                        placeholder           = "Cari Obat"
                        onChangeText          = {(txt) => this.onFilterData(txt)}
                        value                 = {search}
                        round                 = {true}
                        lightTheme            = {true}
                        containerStyle        = {styles.autocompleteContainer}
                        inputContainerStyle   = {styles.container}
                        inputStyle            = {styles.inputText}
                        onClear               = {this.empty}
                        underlineColorAndroid = "transparent"
                    /> */}
                    <View style={styles.autocompleteContainer}>
                        <Item rounded style={styles.container}>
                            <Icon style={{color:'#454F63',marginLeft: 11.79}} name='ios-search'/>
                            <Input 
                                ref             = {input => { this.textInput = input }}
                                onChangeText    = {(txt) => this.onFilterData(txt)}
                                value           = {search}
                                style           = {styles.inputText}
                                placeholder     = 'Cari Obat'
                                clearButtonMode = 'while-editing'/>
                                <Icon onPress={() => this.clearInput()} style={{color:'#D7DEE6',marginRight: 11.79}} name='ios-close-circle'/>
                        </Item>
                    </View>
                    
                    <TouchableWithoutFeedback onPress={() => {
                        navigation.navigate(AppScreen),
                        console.log(this.state.checked),
                        this.empty;
                        this.state.checked
                    }}>
                        <Icon style={styles.cancelButton} name='close'/>
                    </TouchableWithoutFeedback>
                    <View style={{
                        backgroundColor   : '#FFFFFF',
                        position          : 'relative',
                        height            : ITEM_HEIGHT*0.865,
                    }}>
                        <View style={{
                            flexDirection: 'row'}}>
                            
                            <Button full light
                                onPress   = {()=>this.onTabPress(1)}
                                active    = {this.state.activeTab == 1}
                                style={[
                                    styles.button_tab,
                                    {borderBottomWidth    : this.state.activeTab == 1 ? 6:0,
                                    borderBottomColor     : this.state.activeTab == 1 ? '#FFC107':''}]}>
                                    <Text 
                                        style={[
                                            styles.text_tab,
                                            {color: this.state.activeTab == 1 ? '#525C6E':'#959DAD',}]}>
                                            
                                            SEMUA
                                    </Text>
                            </Button>
                            
                            <Button full light
                                onPress   = {()=>this.onTabPress(2)}
                                active    = {this.state.activeTab == 2}
                                style={[
                                    styles.button_tab,
                                    {borderBottomWidth    : this.state.activeTab == 2 ? 6:0,
                                    borderBottomColor     : this.state.activeTab == 2 ? '#FFC107':''}]}>
                                    <Text 
                                        style={[
                                            styles.text_tab,
                                            {color: this.state.activeTab == 2 ? '#525C6E':'#959DAD',}]}>
                                            
                                            GENERIK
                                    </Text>
                            </Button>
                            
                            <Button full light
                                onPress   = {()=>this.onTabPress(3)}
                                active    = {this.state.activeTab == 3}
                                style={{
                                    flex                : 1,
                                    borderBottomWidth   : this.state.activeTab == 3 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 3 ? '#FFC107':''}}>
                                    <Text 
                                        style={[
                                            styles.text_tab,
                                            {color: this.state.activeTab == 3 ? '#525C6E':'#959DAD',}]}>

                                            BRAND
                                    </Text>
                            </Button>
                            
                        </View>

                        <FlatList
                            data                      = {sortedChecked}
                            renderItem                = {this._renderItem}
                            ItemSeparatorComponent    = {this.noUnderLine}
                            onRefresh = {() => console.log("refreshing")}
                            refreshing = {false}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor   : '#FFFFFF',
        width             : 0.8*ITEM_WIDTH,
        height            : 40,
        },
    autocompleteContainer: {
        paddingLeft       : 15,
        left              : 0,
        right             : 0,
        zIndex            : 1,
        backgroundColor   : '#CB1D50',
        height            : 75,
        justifyContent    : 'center',
        borderTopColor    : 'transparent'
    },
    inputText: {
        fontFamily        : 'Nunito-Regular',
        fontSize          : 14,
        color             : '#6C6C6C',
        marginLeft        : 5,
    },
    cancelButton: {
        position          : 'absolute',
        alignItems        : 'flex-end',
        color             : 'white',
        zIndex            : 2,
        right             : 0,
        marginRight       : 23,
        marginTop         : 23,
    },
    itemText: {
        fontSize          : 15,
        margin            : 2
    },
    descriptionContainer: {
        backgroundColor   : '#F5FCFF',
        marginTop         : 25
    },
    infoText: {
        textAlign         : 'center'
    },
    titleText: {
        fontSize          : 18,
        fontWeight        : '500',
        marginBottom      : 10,
        marginTop         : 10,
        textAlign         : 'center'
    },
    directorText: {
        color             : 'grey',
        fontSize          : 12,
        marginBottom      : 10,
        textAlign         : 'center'
    },
    openingText: {
        textAlign         : 'center'
    },
    input: {
        backgroundColor   : 'white',
        height            : 40,
        paddingLeft       : 3
    },
    item_obat: {
        marginTop         : 10,
        height            : ITEM_HEIGHT*0.085,
        width             : ITEM_WIDTH,
        flex              : 1,
    },
    item_nama_obat: {
        fontFamily        : 'Nunito-SemiBold',
        fontSize          : 14,
        color             : '#6C6C6C',
        marginLeft        : 20,
        marginBottom      : 15,
    },
    button_tab: {
        flex              : 1,
        borderRightWidth  : 0.3,
        borderRightColor  : 'rgba(244, 244, 246, 0.5)',
    },
    text_tab: {
        fontFamily        : 'Nunito-Bold',
        fontSize          : 13,
        position          : 'absolute',
    },
    item_tipe_obat: {
        fontFamily        : 'Nunito-SemiBold',
        fontSize          : 14,
        color             : 'rgba(108, 108, 108, 0.7)',
        marginRight       : 20,
        right             : 0,
        position          : 'absolute'
    },
    searchPage_Divider: {
        marginLeft: 10, marginRight: 10, marginTop: 3,
        borderBottomColor: '#EAF4FF',
        borderBottomWidth: 1,
    },
});

export default searchPageB;